#!/bin/sh

LINK="${1:-""}"
NAME="${2:-""}"
SHASUM="${3:-""}"


# check is user asking for help
if [ "$LINK" = "-h" ] || [ "$LINK" = "--help" ] || [ "$LINK" = "help" ]; then
	printf "\
Download Firmware Script
------------------------
Usage  : $ ./program [LINK] [NAME] [SHASUM]

Example: $ ./program \"https://downloads.file.com/.../firmware.bin\" \\
		\"firmware.bin\" \\
		\"1352351234129135235234234\"
"
	exit 0
fi


# validate the parameters
1>&2 printf "[ INFO ] Validating inputs...\n"
if [ "$LINK" = "" ]; then
	1>&2 printf "[ ERROR ] missing download link.\n"
	exit 1
fi
1>&2 printf "[ INFO ] Link is: $LINK\n"

if [ "$NAME" = "" ]; then
	1>&2 printf "[ ERROR ] missing name.\n"
	exit 1
fi
1>&2 printf "[ INFO ] NAME is: $NAME\n"

if [ "$SHASUM" = "" ]; then
	1>&2 printf "[ ERROR ] missing shasum.\n"
	exit 1
fi
1>&2 printf "[ INFO ] SHASUM is: $SHASUM\n"
1>&2 printf "[  OK  ]\n"


# download the file
1>&2 printf "[ INFO ] Downloading file...\n"
wget --quiet --continue "$LINK" --output-document "$NAME"
if [ $? -ne 0 ]; then
	1>&2 printf "[ ERROR ] failed to download file.\n"
	rm "$NAME" &> /dev/null
	exit 1
fi
1>&2 printf "[  OK  ]\n"


# shasum the result
1>&2 printf "[ INFO ] SHA256 summing the downloaded file...\n"
echo "$SHASUM $NAME" | sha256sum --check --strict
if [ $? -ne 0 ]; then
	1>&2 printf "[ ERROR ] bad download image.\n"
	exit 1
fi
1>&2 printf "[  OK  ]\n"


# report result
1>&2 printf "[ SUCCESS ] file downloaded successfully\n"
exit 0
