#!/bin/bash
$FIRMWARE="${1:""}"
$IP="${2:-""}"




# Check for user call for help
if [ "$FIRMWARE" == "-h" ] || \
	[ "$FIRMWARE" == "--help" ] || \
	[ "$FIRMWARE" == "help" ]; then
	printf "\
DLINK RECOVERY BOOTLOADER FLASHER
---------------------------------
USAGE  : $ ./dlink-flash.sh [FIRMWARE_FILEPATH] [IP]

EXAMPLE: $ ./dlink-flash.sh ./path/to/firmware.bin 192.168.0.1
"
	exit 0
fi




# Validate inputs
1>&2 printf "[ INFO ] validating all inputs...\n"
if [ ! -f "$FIRMWARE" ]; then
	1>&2 printf "[ ERROR ] missing firmware.\n"
	exit 1
fi

if [ "$IP" == "" ]; then
	1>&2 printf "[ ERROR ] missing IP.\n"
	exit 1
fi
1>&2 printf "[ OK ]\n"




# perform flashing
1>&2 printf "[ INFO ] submitting firmware now...\n"
curl --verbose \
	--include \
	--request POST \
	--form "firmware=@${FIRMWARE}" \
	"$IP"
if [ $? -ne 0 ]; then
	1>&2 printf "[ FAILED ] HTTP call failed.\n"
	exit 1
fi
1>&2 printf "[ SUCCESS ] The HTTP call went in successfully.\n"
