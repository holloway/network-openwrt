# OpenWRT Firmware Installation (DLink DIR-861 A1)
This document archived how to successfully flash the OpenWRT firmware into
DLink DIR-861 A1 router model. The conventional OpenWRT guide will not work
since the router's bootloader can silently reject non-DLink firmware.

After tested a couple of times, I successfully replicated a number of tricks
from various folks, notably:

1. [tmgoblin](https://forum.openwrt.org/t/d-link-recovery-mode-dir-882-dir-878-dir-867/78758) -
   who detected this issue and attempted an exploit hacks towards it.
2. [CataLpa](https://wzt.ac.cn/2020/08/28/bypass_auth/) - who first found the
   CVE-2020-8864 vulnerabilty that permits the OpenWRT firmware to be flashed
   using the exploit.
3. [dirkomatic](https://forum.openwrt.org/t/d-link-recovery-mode-dir-882-dir-878-dir-867/78758/4) -
   who confirmed that the HTTP call can be workaround using multiple reset.




## Key Problems
There are a number of key problems that forced me to make this repository.



### 1) D-Link Recovery Bootloader Origin Identification
As tmgoblin discovered that D-Link's recovery bootloader is capable of
identifying non-D-Link firmware even in its unecrypted nature. If we follows
Jan 2021 OpenWRT [DLink Recovery GUI Guide](https://openwrt.org/docs/guide-user/installation/installation_methods/d-link_recovery_gui?dataofs=25), you will find that the router
will accept but never flash OpenWRT firmware at all.



### 2) One-Time HTTP Call
A second problem is that the D-Link recovery bootloader only accepts
**ONE and Only** HTTP call which is to **POST** the firmware into that web
portal and its internal system takes over.

Hence, this contradicts the OpenWRT wiki guide where it mentioned that the
web interface is a quick and successful one. In fact, one needs to reset to
bootloader twice in order to make a full HTTP web interface call.




## Tools At Disposal
To flash OpenWRT successfully, you need to ensure you have the following
items at your disposal:

1. Inside this directory has a zip file containing:
  1. the successful firmware (factory).
  2. the OEM firmware containing CVE-2020-8864 vulnerability.
  3. the exploiting python script.
2. The `dlink-flash.sh` script for easy bootloading flashing.
3. A computer system that can telnet for executing flashing command.




## What's Inside `dlink-dir867-a1-firmwares.zip`
There should have 4 files inside:

1. `oem-100b07.bin` - renamed version of OEM firmware (`DIR-882-US_REVA_FIRMWARE_v1.00B07.zip`).
2. `factory.bin` - renamed version of OpenWRT's squashfs-factory.bin downloaded
on Jaunary 23, 2021 at https://openwrt.org/toh/d-link/d-link_dir-867_a1.
3. `sysupgrade.bin` - renamed version of OpenWRT's squashfs-sysupgrade.bin
downloaded on January 23, 2021 at https://openwrt.org/toh/d-link/d-link_dir-867_a1.
You won't be using this firmware anyway.
4. `telnet-exploit.py` - an automated exploit script using Python programming
language.

The firmwares are renamed for the sake of terminal typing friendliness. They
are all downloded with sha256sum verified.

The zip file itself is GPG detached signed using my GPG global key available at:
http://keys.gnupg.net/pks/lookup?op=vindex&fingerprint=on&search=0xCFD3316C29873FB5




## Flashing Steps
Before we start, let's be clear with our objectives in sequences of event:

1. Fallback to OEM firmware version containing CVE-2020-8864 exploit.
2. Perform CVE-2020-8864 to force open a backdoor telnet terminal.
3. Perform base firmware upload and flashing using both OEM web interface and
   telnet.
4. Setup and securing SSH communications after OpenWRT is booted.



### 1. Unpack the Necessary Tools
You need to unpack and setup your workspace. Your computer shall connect to the
router as guided in [OpenWRT D-Link Recovery GUI guide](https://openwrt.org/docs/guide-user/installation/installation_methods/d-link_recovery_gui?dataofs=25)
where:

1. Your computer is connected to the router via ethernet link.
2. Your computer is manually set IP to `192.168.0.XXX` as long as it is not
   `192.168.0.1` (e.g. `192.168.0.2`).
3. Unzip the `dlink-dir867-a1-firmwares.zip` to extract all the tools and
   firmware binaries in place.
4. Take the `scripts/dlink-flash.sh` and get it ready. It makes your life A LOT
   easier.
5. You need an old browser (like old Firefox to avoid the Wizard<-->Login
   pages forever looping problem).
6. Your computer should have telnet installed and ready to use.
7. Your computer should have Python 3 installed (exploit script is python in
   nature).



### 2. Getting Into Recovery Bootloader
As guided in OpenWRT D-Link Recovery GUI guide, to get into recovery bootloader
interface, you:

1. First power down the router.
2. Press and hold the reset button until given instruction.
3. Power on the router. You still hold the reset button.
4. Observe the power LED blink in red color (that means you're in bootloader
   mode).
5. Release the reset button.
6. **DO NOT** visit the web page (you do not want to waste that 1 HTTP call).
   In case you do, you will have to start over from Step 1 (power down router)
   again.



### 3. Flash the CVE-2020-8864 Vulnerable Firmware
Now, with the `dlink-flash.sh` script, you can flash the `oem-100b07.bin`
OEM firmware. The command is something as such:

```
$ ./dlink-flash.sh ./oem-100b07.bin 192.168.0.1
```

You should observe a few things:

1. The router accepts the firmware.
2. Upon successful flashing, the power LED will go off instead of blinking. This
   signals its rebooting.
3. Wait for router to get back online. Now, you should visit the website where
   you will observe the firmware version went fallback to version 1. This is
   exactly what we want with our 1st objective.
4. Try login with your old admin password. If fails, factory reset the router
   and focus on setting a simple password. You will need it for uploading
   openWRT into the router system.
5. Repeat Step 4 until you have a stable access into it.
6. Once inside, head over to the page where you can perform firmware update.

> Just in case you bump into the webpage where it keeps looping between login
> and wizard, switch to older browser like old Firefox to prevent that looping.
>
> I only encounter that problem on Google Chrome so I assume all Chrome-based
> browsers are affected in a similar manner.



### 4. Exploit the CVE-2020-8864 Router To Open Telnet Backdoor
This step is crucial where you are attacking the router firmware using that
`telnet-exploit.py` python script. To attack, simply run the script with
the IP address as its parameter. The command is something as such:

```
$ python ./telnet-exploit.py 192.168.0.1
```

Once successful, the script will tell you that telnet is OK and ready to be
accessed. Then, try telnet into the router:

```
$ telnet 192.168.0.1
```

If you get a busybox terminal, you're inside the router now. Congrats on
achieving the 2nd objective.

> If you failed this step, start over again by repeating the previous step.
> At the end of the day, you must have the firmware update web page and
> connected telnet ready before proceeding to next step.



### 5. Upload the OpenWRT Factory Firmware
Head back to the webpage and upload the `factory.bin`. This will upload the
OpenWRT firmware into the router as tmgoblin discovered. You're expecting the
upload result to **fail** so do not be alarmed about it.

Then, head over to telnet and look for the uploaded firmware:

```
$ ls /tmp/firmware.img
/tmp/firmware.img
```

It should be there due to the vulnerability and `ls` will repeat the path.



### 6. Flash the OpenWRT Factory Firmware
With everything in placed, you can proceed to flash it via telnet command:

```
$ mtd_write -r -w write /tmp/firmware.img Kernel
```

This will flash the firmware to the point where telnet lost its update and
possibly closed down. This means that the router will use the OpenWRT firmware
on next reboot.



### 7. Remove Static IP and Check with SSH
Upon returning to ready mode, you shall remove your manually added IP from
your computer. Reset your computer's network so that it can reterive a DHCP
leasing IP from your router.

With the connected IP, try connect into it using SSH:

```
$ ssh root@[RouterIP]
```

If you can login, you had done it successfully.



### 8. Get Router Connect to Internet
Now OpenWRT needs Internet access in order to manage its packages. That will be
covered outside of this document.




## Epilogue
Congrats for having your D-Link DIR-867 router flashed with OpenWRT. I hope you
too enjoy your first script-kiddies attack onto a router alongside the journey
as well.

Please enjoy your new router.




## Resources
Here are the archived resources in case of future needs.

1. DLink Version 1.00 B07 Firmware (ftp://ftp2.dlink.com/PRODUCTS/DIR-867/REVA/)
2. DLink Recovery Mode Forum Discussion (https://forum.openwrt.org/t/d-link-recovery-mode-dir-882-dir-878-dir-867/78758i)
3. OpenWRT D-Link Recovery GUI Guide (https://openwrt.org/docs/guide-user/installation/installation_methods/d-link_recovery_gui?dataofs=25)
4. OpenWRT Generic Installation Guide (https://openwrt.org/docs/guide-user/installation/generic.flashing#installation_checklist)
5. OpenWRT - DLink DIR 867 A1 Model (https://openwrt.org/toh/d-link/d-link_dir-867_a1)
6. CataLpa Document (https://wzt.ac.cn/2020/08/28/bypass_auth/)
